import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { QuillEditorModule } from 'ngx-quill-editor';

import { AppComponent } from './app.component';
import { IndexComponent } from './route/index/index.component';
import { BoardComponent } from './route/board/board.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { FooterComponent } from './component/footer/footer.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ArticleComponent } from './route/board/article/article.component';
import { WeareComponent } from './route/introduce/weare/weare.component';
import { CategoryComponent } from './route/introduce/category/category.component';
import { RgBoardCardSummaryComponent } from './component/rg-board-card-summary/rg-board-card-summary.component';
import { RgCarouselComponent } from './component/rg-carousel/rg-carousel.component';
import { RgCarousel2Component } from './component/rg-carousel2/rg-carousel2.component';
import { UpRiseMotionDirective } from './directive/up-rise-motion.directive';
import { RgPostEditorComponent } from './component/rg-post-editor/rg-post-editor.component';
import { RgFileUploaderComponent } from './component/rg-file-uploader/rg-file-uploader.component';
import { EditComponent } from './route/board/edit/edit.component';
import { AddComponent } from './route/board/add/add.component';
import { LoginComponent } from './route/auth/login/login.component';
import { JoinComponent } from './route/auth/join/join.component';
import { SitemapComponent } from './route/sitemap/sitemap.component';
import { RgSessionService } from './composer/rg-commons/rg-session/rg-session.service';
import { FacebookModule } from 'ngx-facebook';
import { LogoutComponent } from './route/auth/logout/logout.component';
import { HeaderInterceptorService } from './header-interceptor.service';
import { PydateConvertDirective } from './directive/pydate-convert.directive';
import { PrivateComponent } from './route/private/private.component';
import { AgreeComponent } from './route/agree/agree.component';
import { ContactUsComponent } from './route/contact-us/contact-us.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    BoardComponent,
    NavbarComponent,
    FooterComponent,
    ArticleComponent,
    WeareComponent,
    CategoryComponent,
    RgBoardCardSummaryComponent,
    RgCarouselComponent,
    RgCarousel2Component,
    UpRiseMotionDirective,
    RgPostEditorComponent,
    RgFileUploaderComponent,
    EditComponent,
    AddComponent,
    LoginComponent,
    JoinComponent,
    SitemapComponent,
    LogoutComponent,
    PydateConvertDirective,
    PrivateComponent,
    AgreeComponent,
    ContactUsComponent
  ],
  imports: [
    BrowserModule,
    // BrowserModule.withServerTransition({ appId: 'rg-web-proto' }),
    BrowserAnimationsModule,
    // TransferHttpCacheModule,
    HttpClientModule,
    FacebookModule.forRoot(),
    FormsModule, ReactiveFormsModule,
    QuillEditorModule,
    RouterModule.forRoot([
      {
        path: '',
        component: IndexComponent
      }, {
        path: 'agree',
        component: AgreeComponent
      }, {
        path: 'private',
        component: PrivateComponent
      }, {
        path: 'introduce',
        children: [
          {
            path: 'weare',
            component: WeareComponent
          },
          {
            path: 'category',
            component: CategoryComponent
          }, {
            path: 'contact-us',
            component: ContactUsComponent
          }
        ]
      }, {
        path: 'auth',
        children: [
          {
            path: 'login',
            component: LoginComponent
          }, {
            path: 'join',
            component: JoinComponent
          }
        ]
      }, {
        path: 'board',
        children: [{
          path: ':boardName/article/add',
          component: AddComponent
        }, {
          path: ':boardName/article/:articleCid/edit',
          component: EditComponent
        }, {
          path: ':boardName',
          component: BoardComponent,
          children: [{
            path: 'article/:articleCid',
            component: ArticleComponent
          }]
        }]
      }
    ], { anchorScrolling: 'enabled' })
  ],
  providers: [
    RgSessionService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HeaderInterceptorService,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    private rgSessionService: RgSessionService
  ) {
    this.rgSessionService.getSession();
  }
}

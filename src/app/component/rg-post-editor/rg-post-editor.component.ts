import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { RgArticle } from 'src/app/composer/rg-commons/rg-board/rg-board';
import { RgCommonReaderService } from 'src/app/composer/rg-commons/rg-board/rg-common-reader.service';

@Component({
  selector: 'app-rg-post-editor',
  templateUrl: './rg-post-editor.component.html',
  styleUrls: ['./rg-post-editor.component.css']
})
export class RgPostEditorComponent implements OnInit {
  @Input() article: RgArticle;
  @Input() boardName?: string;
  @ViewChild('searchTagList') searchTagList: ElementRef;
  @ViewChild('tagInput') tagInput: ElementRef;
  editorOptions = {};
  searchedTagList = [];
  constructor(
    private rgCommonReaderService: RgCommonReaderService
  ) { }

  ngOnInit() {
  }
  onTagDelete(tagIndex) {
    this.article.tagList.splice(tagIndex, 1);
  }
  onTagInputChange($event) {
    if ($event.key === 'Enter') {
      this.onTagEnter($event);
    } else {
      if ($event.target.value.length > 1) {
        this.searchedTagList = [];
        this.rgCommonReaderService.getTagList(this.boardName, $event.target.value).subscribe(response => {
          this.searchedTagList = response['response'];
          if (this.searchedTagList.length > 0) {
            this.searchTagList.nativeElement.style.display = 'block';
          } else {
            this.searchTagList.nativeElement.style.display = 'none';
          }
        });
      } else {
        this.searchTagList.nativeElement.style.display = 'none';
      }
    }
  }
  selectTag(index) {
    if (!this.article.tagList) {
      this.article.tagList = [];
    }
    this.article.tagList.push(this.searchedTagList[index]);
    this.searchTagList.nativeElement.style.display = 'none';
    this.tagInput.nativeElement.value = '';
  }
  onTagEnter($event) {
    if ($event.target.value.length > 1) {
      if (!this.article.tagList) {
        this.article.tagList = [];
      }
      if (this.article.tagList.indexOf($event.target.value) < 0) {
        this.article.tagList.push($event.target.value);
        $event.target.value = '';
        this.searchTagList.nativeElement.style.display = 'none';
      } else {
        alert('이미 입력된 태그입니다.');
      }
    } else {
      alert('태그를 두자 이상 입력해주시기 바랍니다.');
    }
  }
}

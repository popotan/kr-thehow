import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RgBoardCardSummaryComponent } from './rg-board-card-summary.component';

describe('RgBoardCardSummaryComponent', () => {
  let component: RgBoardCardSummaryComponent;
  let fixture: ComponentFixture<RgBoardCardSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RgBoardCardSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RgBoardCardSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';
import { apiURL } from 'src/environments/environment.prod';

@Component({
  selector: 'app-rg-board-card-summary',
  templateUrl: './rg-board-card-summary.component.html',
  styleUrls: ['./rg-board-card-summary.component.css']
})
export class RgBoardCardSummaryComponent implements OnInit {
  @Input() article;
  @Input() boardName;

  constructor() { }

  ngOnInit() {
  }

  getImageUrlFromFileList() {
    for (const file of this.article.fileList) {
      if (['png', 'jpg', 'jpeg', 'gif'].indexOf(file.type.toLowerCase()) > -1) {
        return apiURL + '/board/file/' + file.id;
        break;
      }
    }
    return '/assets/images/no-image.png';
  }

}

import { Component, OnInit } from '@angular/core';
import { RgSessionService } from 'src/app/composer/rg-commons/rg-session/rg-session.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user = this.rgSessionService.getUser();
  constructor(
    private router: Router,
    public rgSessionService: RgSessionService
  ) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        window.scroll(0, 0);
      }
    });
  }

  ngOnInit() {
  }

}

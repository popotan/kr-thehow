import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RgFileUploaderComponent } from './rg-file-uploader.component';

describe('RgFileUploaderComponent', () => {
  let component: RgFileUploaderComponent;
  let fixture: ComponentFixture<RgFileUploaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RgFileUploaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RgFileUploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Output, EventEmitter, ViewChild, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { apiURL } from 'src/environments/environment.prod';
import { RgFileUploaderService } from 'src/app/composer/rg-commons/rg-board/rg-file-uploader.service';
import { RgFile } from 'src/app/composer/rg-commons/rg-board/rg-board';

@Component({
  selector: 'app-rg-file-uploader',
  templateUrl: './rg-file-uploader.component.html',
  styleUrls: ['./rg-file-uploader.component.css']
})
export class RgFileUploaderComponent implements OnInit {
  // tslint:disable-next-line:no-output-on-prefix
  @Output('onFileUploadComplete') onFileUploadComplete: EventEmitter<any> = new EventEmitter();
  @ViewChild('uploadedFiles') uploadedFiles;
  @Input() uploadedFileList: RgFile[];
  selectedFilesIndex = [];
  isUploading = false;
  progress = 0;

  constructor(
    private activatedRoute: ActivatedRoute,
    public rgFileUploadService: RgFileUploaderService
  ) {
    this.rgFileUploadService.progress$.subscribe(value => {
      this.progress = value;
    });
  }

  ngOnInit() {
  }

  onSelectionChange($event) {
    const index = $event.target.value;
    if (this.selectedFilesIndex.indexOf(index) > -1) {
      this.selectedFilesIndex.splice(this.selectedFilesIndex.indexOf(index), 1);
    } else {
      this.selectedFilesIndex.push(index);
    }
  }

  removeFiles() {
    for (let i = 0; i < this.selectedFilesIndex.length; i++) {
      this.uploadedFileList.splice(this.selectedFilesIndex.pop() - i, 1);
    }
    this.onFileUploadComplete.emit(this.uploadedFileList);
  }

  uploadFiles($event) {
    this.isUploading = true;
    this.rgFileUploadService.uploadFile(
      apiURL + '/board/' + this.activatedRoute.snapshot.params['boardName'] + '/file',
      $event.target.files).then(response => {
        response['response'].forEach(file => {
          const rgFile = new RgFile();
          rgFile.name = file.filename;
          rgFile.size = file.filesize_mb;
          rgFile.realPath = file.path;
          this.uploadedFileList.push(rgFile);
        });
        this.isUploading = false;
      });
    this.onFileUploadComplete.emit(this.uploadedFileList);
  }
}

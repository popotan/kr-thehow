import {
  Component, OnInit, Input, AfterViewInit,
  ViewChildren, ViewContainerRef, TemplateRef, ViewChild, Renderer2, ElementRef, OnDestroy, HostListener
} from '@angular/core';
import { ComponentFactoryResolver } from '@angular/core';
import { RgCarousel } from 'src/app/composer/rg-commons/rg-carousel/rg-carousel';

@Component({
  selector: 'app-rg-carousel',
  templateUrl: './rg-carousel.component.html',
  styleUrls: ['./rg-carousel.component.css']
})
export class RgCarouselComponent implements OnInit, AfterViewInit, OnDestroy {
  posTop;
  __rgCarouselFactory: RgCarousel;
  slideCounter: any;
  @Input() set rgCarouselFactory(rgCarousel: RgCarousel) {
    this.__rgCarouselFactory = rgCarousel;
    if (rgCarousel.cssWidth) {
      this.renderer2.setStyle(this.carousel.nativeElement, 'width', this.__rgCarouselFactory.cssWidth);
    }
    if (rgCarousel.cssHeight) {
      this.renderer2.setStyle(this.carousel.nativeElement, 'height', this.__rgCarouselFactory.cssHeight);
    }
  }
  @ViewChild('carousel') carousel;
  __currentIndex = 0;
  set currentIndex(index: number) {
    const movePercent = -100 * index;
    this.renderer2.setStyle(this.carouselSlide.nativeElement, 'transform', `translateX(${movePercent}%)`);
    this.__currentIndex = index;
  }
  __fixedSlideTemplateRef = [];
  __slideTemplateRef = [];
  @ViewChildren('fixedContainer', { read: ViewContainerRef }) fixedContainers;
  @ViewChildren('slideContainer', { read: ViewContainerRef }) slideContainers;
  @Input('slides') set slides(templates: TemplateRef<any>[]) {
    this.__slideTemplateRef = [];
    templates.forEach(element => {
      this.__slideTemplateRef.push(element);
    });
  }
  @Input('fixedSlides') set fixedSlides(templates: TemplateRef<any>[]) {
    this.__fixedSlideTemplateRef = [];
    templates.forEach(element => {
      this.__fixedSlideTemplateRef.push(element);
    });
  }
  @ViewChild('carouselSlide', { read: ElementRef }) carouselSlide;
  constructor(
    public renderer2: Renderer2,
    public cfr: ComponentFactoryResolver,
    public el: ElementRef
  ) { }
  @Input() allowFixedSlide = false;
  @Input() allowSlide = true;
  ngOnInit() {
  }

  ngAfterViewInit() {
    this.posTop = this.el.nativeElement.getBoundingClientRect().top;
    this.slideContainers.forEach((container, index) => {
      container.createEmbeddedView(this.__slideTemplateRef[index]);
    });
    this.fixedContainers.forEach((container, index) => {
      container.createEmbeddedView(this.__fixedSlideTemplateRef[index]);
    });
    this.__currentIndex = 0;
    if ((window.scrollY + window.innerHeight) >= this.posTop) {
      this.postIntervalChange();
    }
  }

  ngOnDestroy() {
    clearInterval(this.slideCounter);
  }

  @HostListener('document:scroll', ['$event'])
  onWindowScroll($event) {
    if ((window.scrollY + window.innerHeight) >= this.posTop) {
      this.postIntervalChange();
    } else {
      if (this.slideCounter) {
        clearInterval(this.slideCounter);
      }
    }
  }

  postIntervalChange() {
    if (this.slideCounter) {
      clearInterval(this.slideCounter);
    }
    this.slideCounter = setInterval(function () {
      this.moveTo(this.__currentIndex + 1);
    }.bind(this), this.__rgCarouselFactory.slideTimer);
  }

  moveTo(targetIndex: number) {
    if (this.allowSlide) {
      if (targetIndex >= this.__slideTemplateRef.length) {
        this.currentIndex = 0;
      } else if (targetIndex < 0) {
        this.currentIndex = this.__slideTemplateRef.length - 1;
      } else {
        this.currentIndex = targetIndex;
      }
    }

    if (this.allowFixedSlide) {
      if (targetIndex >= this.__fixedSlideTemplateRef.length) {
        this.currentIndex = 0;
      } else if (targetIndex < 0) {
        this.currentIndex = this.__fixedSlideTemplateRef.length - 1;
      } else {
        this.currentIndex = targetIndex;
      }
    }

    this.postIntervalChange();
  }

}

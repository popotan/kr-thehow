import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RgCarousel2Component } from './rg-carousel2.component';

describe('RgCarousel2Component', () => {
  let component: RgCarousel2Component;
  let fixture: ComponentFixture<RgCarousel2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RgCarousel2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RgCarousel2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

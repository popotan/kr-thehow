import { Component, OnInit, Renderer2, ComponentFactoryResolver, ElementRef } from '@angular/core';
import { RgCarouselComponent } from '../rg-carousel/rg-carousel.component';

@Component({
  selector: 'app-rg-carousel2',
  templateUrl: './rg-carousel2.component.html',
  styleUrls: ['./rg-carousel2.component.css']
})
export class RgCarousel2Component extends RgCarouselComponent implements OnInit {
  __fixedSlideTemplateRef = [null, null, null, null];
  __slideTemplateRef = [null, null, null, null];
  constructor(
    public renderer2: Renderer2,
    public cfr: ComponentFactoryResolver,
    public el:ElementRef
  ) {
    super(renderer2, cfr, el);
  }

  ngOnInit() {
  }

  set currentIndex(index: number) {
    const movePercent = -20 * index;
    this.renderer2.setStyle(this.carouselSlide.nativeElement, 'transform', `translateX(${movePercent}%)`);
    this.__currentIndex = index;
  }

}

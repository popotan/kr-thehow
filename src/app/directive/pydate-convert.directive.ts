import { Directive, Input, AfterViewInit, ElementRef } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[pydate]'
})
export class PydateConvertDirective implements AfterViewInit {

  constructor(
    private el: ElementRef
  ) { }

  @Input() format;
  __pyDate;
  @Input('pydate') set pydate(value) {
    this.__pyDate = value;
    this.jsDate = new Date(value);
    this.yyyy = this.jsDate.getFullYear();
    this.mm = this.jsDate.getMonth() + 1;
    this.dd = this.jsDate.getDate();
    this.hh = this.jsDate.getHours();
    this.min = this.jsDate.getMinutes();
    this.sec = this.jsDate.getSeconds();
  }
  jsDate;
  resultDate;
  yyyy;
  mm;
  dd;
  hh;
  min;
  sec;

  ngAfterViewInit() {
    if (this.format && this.__pyDate) {
      this.convertPydateToFormat();
      this.el.nativeElement.innerHTML = this.resultDate;
    } else if (this.format) {
      this.jsDate = new Date();
      this.yyyy = this.jsDate.getFullYear();
      this.mm = this.jsDate.getMonth() + 1;
      this.dd = this.jsDate.getDate();
      this.hh = this.jsDate.getHours();
      this.min = this.jsDate.getMinutes();
      this.sec = this.jsDate.getSeconds();
      this.convertPydateToFormat();
      this.el.nativeElement.innerHTML = this.resultDate;
    }
  }

  convertPydateToFormat() {
    // tslint:disable-next-line:prefer-const
    let temp = this.format.split(' ');
    for (let i = 0; i < temp.length; i++) {
      temp[i] = temp[i].split('').reverse();
    }

    if (temp.length > 0) { // y,m,d 영역
      // y치환
      let yCount = 0, mCount = 0, dCount = 0;
      for (let i = 0; i < temp[0].length; i++) {
        if (temp[0][i] === 'y') {
          const getTargetIndex = this.yyyy.toString().length - yCount - 1;
          if (getTargetIndex > -1) {
            temp[0][i] = this.yyyy.toString()[getTargetIndex];
          } else {
            temp[0][i] = '0';
          }
          yCount++;
        } else if (temp[0][i] === 'm') {
          const getTargetIndex = this.mm.toString().length - mCount - 1;
          if (getTargetIndex > -1) {
            temp[0][i] = this.mm.toString()[getTargetIndex];
          } else {
            temp[0][i] = '0';
          }
          mCount++;
        } else if (temp[0][i] === 'd') {
          const getTargetIndex = this.dd.toString().length - dCount - 1;
          if (getTargetIndex > -1) {
            temp[0][i] = this.dd.toString()[getTargetIndex];
          } else {
            temp[0][i] = '0';
          }
          dCount++;
        }
      }
    }
    if (temp.length > 1) { // h,m,s 영역
      let hCount = 0, mCount = 0, sCount = 0;
      for (let i = 0; i < temp[1].length; i++) {
        if (temp[1][i] === 'h') {
          const getTargetIndex = this.hh.toString().length - hCount - 1;
          if (getTargetIndex > -1) {
            temp[1][i] = this.hh.toString()[getTargetIndex];
          } else {
            temp[1][i] = '0';
          }
          hCount++;
        } else if (temp[1][i] === 'm') {
          const getTargetIndex = this.min.toString().length - mCount - 1;
          if (getTargetIndex > -1) {
            temp[1][i] = this.min.toString()[getTargetIndex];
          } else {
            temp[1][i] = '0';
          }
          mCount++;
        } else if (temp[1][i] === 's') {
          const getTargetIndex = this.sec.toString().length - sCount - 1;
          if (getTargetIndex > -1) {
            temp[1][i] = this.sec.toString()[getTargetIndex];
          } else {
            temp[1][i] = '0';
          }
          sCount++;
        }
      }
    }
    if (temp.length > 2) {
      console.log('error');
    }
    for (let i = 0; i < temp.length; i++) {
      temp[i] = temp[i].reverse().join('');
    }
    this.resultDate = temp.join(' ');
  }
  // addZeroString(length, target) {
  //   const targetStringArray = target.split('');
  //   // tslint:disable-next-line:prefer-const
  //   let temp = [];
  //   const lengthInterval = length - targetStringArray.length;
  //   for (let i = 0; i < lengthInterval; i++) {
  //     temp.push('0');
  //   }
  //   let result = temp.join('');

  //   if (lengthInterval < 0) {
  //     for (let k = -lengthInterval; k < targetStringArray.length; k++) {
  //       result += targetStringArray[k];
  //     }
  //   }
  //   return result;
  // }
}

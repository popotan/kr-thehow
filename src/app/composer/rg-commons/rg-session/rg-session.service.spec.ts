import { TestBed } from '@angular/core/testing';

import { RgSessionService } from './rg-session.service';

describe('RgSessionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RgSessionService = TestBed.get(RgSessionService);
    expect(service).toBeTruthy();
  });
});

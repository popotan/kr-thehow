import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthResponse, FacebookService } from 'ngx-facebook';
import { apiURL } from 'src/environments/environment.prod';
import { Subscribable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RgSessionHttpService {

  constructor(
    private $http: HttpClient,
    private facebookService: FacebookService
  ) { }

  getFacebookUserInfoFromGraphApi(authResponse: AuthResponse): Subscribable<any> {
    return this.$http.get(
      `https://graph.facebook.com/v2.11/${authResponse.userID}?`
      + `fields=id,name,email,picture.width(160).height(160).type(square)`
      + `&access_token=${authResponse.accessToken}`)
      .pipe();
  }

  postSocialSessionToApi(social: string, response: object): Subscribable<any> {
    return this.$http.post(apiURL + `/cuser/session/${social}`, response)
      .pipe(debounceTime(1000), distinctUntilChanged());
  }

  getApiSession(): Subscribable<any> {
    return this.$http.get(apiURL + `/cuser/session`)
      .pipe();
  }

  removeApiSession(): Subscribable<any> {
    return this.$http.delete(apiURL + `/cuser/session`)
      .pipe(debounceTime(1000), distinctUntilChanged());
  }

  login(user): Subscribable<any> {
    return this.$http.post(apiURL + `/cuser/session/email`, user)
      .pipe(debounceTime(1000), distinctUntilChanged());
  }

  join(user): Subscribable<any> {
    return this.$http.post(apiURL + `/cuser/join/email`, user)
      .pipe(debounceTime(1000), distinctUntilChanged());
  }
}

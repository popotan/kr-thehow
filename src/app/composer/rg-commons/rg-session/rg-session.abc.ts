namespace abcRgSession {
    export interface User {
        uid?: number;
        incomingType: IncomingType;
        email?: string;
        profileImageUrl?: string;
    }
    export const enum IncomingType {
        FACEBOOK = 'FACEBOOK',
        NAVER = 'NAVER',
        GOOGLE = 'GOOGLE',
        EMAIL = 'EMAIL'
    }
    export const enum PermissionType {
        admin = 'admin',
        guest = 'guest'
    }
    export interface FacebookGraphAPIResponse {
        id: string;
        name: string;
        email: string;
        profileImageUrl: string;
    }
}

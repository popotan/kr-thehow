
export class User {
    uid?: number;
    incomingType: abcRgSession.IncomingType;
    name: string;
    email?: string;
    profileImageUrl?: string;
    auth?: string[];
}
export class LoginUserForm implements abcRgSession.User {
    uid?: number;
    incomingType: abcRgSession.IncomingType;
    name: string;
    email?: string;
    profileImageUrl?: string;
    password: string;
}
export class JoinUserForm {
    name: string;
    password: string;
    email: string;
    phone: string;
    isAgree: string;
}

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User, LoginUserForm, JoinUserForm } from './rg-session';
import { FacebookService } from 'ngx-facebook';
import { RgSessionHttpService } from './rg-session-http.service';
import { Router } from '@angular/router';

@Injectable()
export class RgSessionService {
  user$: BehaviorSubject<User> = new BehaviorSubject(new User());
  constructor(
    private rgSessionHttpService: RgSessionHttpService,
    private facebookService: FacebookService,
    private router: Router
  ) {}

  getSession() {
    this.rgSessionHttpService.getApiSession().subscribe(response => {
      if (response['meta']['message'] === 'SUCCESS') {
        const user = new User();
        user.email = response['response']['email'];
        user.incomingType = response['response']['division'];
        user.name = response['response']['name'];
        user.uid = response['response']['guest_id'];
        user.auth = response['response']['auth'];
        this.user$.next(user);
      }
    });
  }

  getUser() {
    return this.user$.asObservable();
  }

  getFacebookSessionStatus() {
    this.facebookService.login({
      scope: 'public_profile,email',
      return_scopes: true,
      enable_profile_selector: true
    }).then(response => {
      return response;
    }).catch();
  }

  postSocialSessionToApi(social: string, response: object) {
    this.rgSessionHttpService.postSocialSessionToApi(social, response).subscribe(response2 => {
      return response2;
    });
  }

  login(user: LoginUserForm, redirectUrl?: string) {
    this.rgSessionHttpService.login(JSON.parse(JSON.stringify(user))).subscribe(response => {
      if (response['meta']['message'] === 'SUCCESS') {
        this.getSession();
        if (redirectUrl) {
          this.router.navigateByUrl(redirectUrl);
        } else {
          this.router.navigateByUrl('/');
        }
      } else if (response['meta']['message'] === 'PASSWORD_IS_NOT_MATCHED') {
        alert('비밀번호가 일치하지 않습니다. 다시 시도 해 주시기 바랍니다.');
      } else if (response['meta']['message'] === 'USER_IS_NOT_EXIST') {
        alert('존재하지 않는 이메일 계정입니다. 다시 시도 해 주시기 바랍니다.');
      } else {
        alert('알 수 없는 문제가 발생했습니다. 관리자에게 문의 해 주시기 바랍니다.');
      }
    });
  }

  join(user: JoinUserForm) {
    this.rgSessionHttpService.join(JSON.parse(JSON.stringify(user))).subscribe(response => {
      if (response['meta']['message'] === 'SUCCESS') {
        alert('가입이 정상적으로 처리되었습니다.');
        this.router.navigateByUrl('/auth/login');
      } else if (response['meta']['message'] === 'ALREADY_EXIST_EMAIL') {
        alert('이미 존재하는 이메일입니다. 다시 한 번 확인해주세요.');
      } else {
        alert('가입에 문제가 발생했습니다. 관리자에게 문의 해 주시기 바랍니다.');
      }
    });
  }
}

import { TestBed } from '@angular/core/testing';

import { RgSessionHttpService } from './rg-session-http.service';

describe('RgSessionHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RgSessionHttpService = TestBed.get(RgSessionHttpService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { RgFileUploaderService } from './rg-file-uploader.service';

describe('RgFileUploaderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RgFileUploaderService = TestBed.get(RgFileUploaderService);
    expect(service).toBeTruthy();
  });
});

export class RgBoard implements abcRgBoard.RgBoard {
    id?: number;
    name: string;
    aka: string;
    style: abcRgBoard.BoardStyle;
    langCode: abcRgBoard.LangCode;
    availableLangCode: object[];
    permissions: abcRgSession.PermissionType[];
    articles: abcRgBoard.RgArticle[];
    constructor(availableLangCodeList: object[]) {
        this.availableLangCode = availableLangCodeList;
    }
    public setLangCode(langCode: abcRgBoard.LangCode): void {
        this.langCode = abcRgBoard.LangCode.ko;
        this.setAka();
    }
    setAka(): void {
        if (this.availableLangCode && this.langCode) {
            for (const iterator of this.availableLangCode) {
                if (iterator.hasOwnProperty('aka') &&
                    iterator.hasOwnProperty('lang_code') &&
                    iterator['lang_code'] === this.langCode) {
                    this.aka = iterator['aka'];
                    break;
                }
            }
        }
    }
}
export class RgArticle implements abcRgBoard.RgArticle {
    cid?: number;
    title: string;
    description: string;
    tagList?: string[];
    fileList?: abcRgBoard.RgFile[];
    langCode: string;
    guest?: abcRgSession.User;
    in_private?: boolean;
    is_notice?: boolean;
    popup?: boolean;
    protected __regDate?: Date;
    set regDate(value) {
        this.__regDate = this.parseRegDate(value);
    }
    get regDate() {
        return this.__regDate;
    }
    __replyLength?: number;
    set replyLength(value) {
        this.__replyLength = value;
    }
    get replyLength() {
        return this.__replyLength;
    }
    viewCount?: number;
    constructor() {
    }
    parseRegDate(date) {
        if (date) {
            if (typeof date === 'string') {

            }
        }
        return date;
    }
}
export class RgFile implements abcRgBoard.RgFile {
    id?: number;
    name: string;
    size?: number;
    url?: string;
    type?: string;
    realPath?: string;
    regDate?: string | Date;
    getUrl(): any {
        return '';
    }
}

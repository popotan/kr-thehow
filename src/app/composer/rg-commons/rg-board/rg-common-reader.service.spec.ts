import { TestBed } from '@angular/core/testing';

import { RgCommonReaderService } from './rg-common-reader.service';

describe('RgCommonReaderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RgCommonReaderService = TestBed.get(RgCommonReaderService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { RgArticle } from './rg-board';

@Injectable({
  providedIn: 'root'
})
export class RgFeedService {
  article$: BehaviorSubject<RgArticle>;
  constructor() { }
}

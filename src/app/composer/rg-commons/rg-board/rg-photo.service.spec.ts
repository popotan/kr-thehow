import { TestBed } from '@angular/core/testing';

import { RgPhotoService } from './rg-photo.service';

describe('RgPhotoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RgPhotoService = TestBed.get(RgPhotoService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { RgThreadService } from './rg-thread.service';

describe('RgThreadService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RgThreadService = TestBed.get(RgThreadService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RgFileUploaderService {
  progress$;
  upload_progress = 0;
  upload_progress_observer;

  constructor() {
    this.progress$ = Observable.create(observer => {
      this.upload_progress_observer = observer;
    });
   }

  uploadFile(targetURL, fileList: FileList): Promise<any> {
    const p = new Promise((resolve, reject) => {
      const files: FileList = fileList;
      const formData: FormData = new FormData();
      for (let i = 0; i < files.length; i++) {
        formData.append('upload_files[]', files[i], files[i].name);
      }

      const xhr: XMLHttpRequest = new XMLHttpRequest();
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      };

      xhr.upload.onprogress = (event) => {
        this.upload_progress = Math.round(event.loaded / event.total * 100);
        this.upload_progress_observer.next(this.upload_progress);
      };

      xhr.open('POST', targetURL, true);
      xhr.setRequestHeader('ServiceDomain', window.location.host);
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      xhr.send(formData);
    });

    return p;
  }
}

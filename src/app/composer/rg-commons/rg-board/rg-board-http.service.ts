import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiURL } from 'src/environments/environment.prod';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class RgBoardHttpService {

  constructor(
    private $http: HttpClient
  ) { }
  getBoardInfo(boardName) {
    return this.$http.get(`${apiURL}/board/${boardName}/info`).pipe();
  }
  getArticleList(boardName, page, keyword?, keywordType?, tag?, lang?) {
    let keywordParams = '';
    if (keyword && keywordType) {
      keywordParams += '&keyword=' + keyword + '&keyword_type=' + keywordType;
    }
    let langParams = '';
    if (lang) {
      langParams += `&lang=${lang}`;
    } else {
      langParams += `&lang=ko`;
    }
    let tagParams = '';
    if (tag) {
      tagParams += `&tag=${tag}`;
    }
    return this.$http.get(apiURL + '/board/' + boardName + '?page=' + page + langParams + keywordParams + tagParams)
      .pipe();
  }
  getArticle(boardName, articleCid) {
    return this.$http.get(`${apiURL}/board/${boardName}/${articleCid}`).pipe();
  }
  postArticle(boardName, article) {
    const articleJSON = {
      'title': article.title,
      'description': article.description,
      'lang_code': article.langCode,
      'in_private': false,
      'is_notice': false,
      'popup': false,
      'tag_list': article.tagList,
      'file_list': (fileList => {
        const result = [];
        fileList.forEach(file => {
          const fileJSON = {
            'filename': file.name,
            'path': file.realPath,
            'filesize_mb': file.size
          };
          result.push(fileJSON);
        });
        return result;
      })(article.fileList)
    };
    return this.$http.post(`${apiURL}/board/${boardName}`, articleJSON).pipe();
  }
  putArticle(boardName, articleCid, article) {
    const articleJSON = {
      'title': article.title,
      'description': article.description,
      'lang_code': article.langCode,
      'in_private': false,
      'is_notice': false,
      'popup': false,
      'tag_list': article.tagList,
      'file_list': (fileList => {
        const result = [];
        fileList.forEach(file => {
          if (file.id) {
            const fileJSON = {
              'id': file.id,
              'filename': file.name,
              'filetype': file.type,
              'filesize_mb': file.size,
              'reg_date': file.regDate
            };
            result.push(fileJSON);
          } else {
            const fileJSON = {
              'filename': file.name,
              'path': file.realPath,
              'filesize_mb': file.size
            };
            result.push(fileJSON);
          }
        });
        return result;
      })(article.fileList)
    };
    return this.$http.put(`${apiURL}/board/${boardName}/modify/${articleCid}`, articleJSON)
      .pipe(debounceTime(1000), distinctUntilChanged());
  }
  deleteArticle(boardName, articleCid) {
    return this.$http.delete(`${apiURL}/board/${boardName}/${articleCid}`)
      .pipe(debounceTime(1000), distinctUntilChanged());
  }
  getTags(boardName, keyword?) {
    let keywordParams = '';
    if (keyword) {
      keywordParams = `?keyword=${keyword}`;
    }
    return this.$http.get(`${apiURL}/board/${boardName}/tags${keywordParams}`)
      .pipe(debounceTime(1000), distinctUntilChanged());
  }
}

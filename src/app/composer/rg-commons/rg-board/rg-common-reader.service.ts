import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { RgArticle, RgFile } from './rg-board';
import { RgBoardHttpService } from './rg-board-http.service';
import { User } from '../rg-session/rg-session';

@Injectable()
export class RgCommonReaderService {
  meta$: BehaviorSubject<any> = new BehaviorSubject(null);
  article$: BehaviorSubject<RgArticle> = new BehaviorSubject(null);
  constructor(
    private rgBoardHttpService: RgBoardHttpService
  ) { }
  getArticle(boardName, articleCid) {
    this.rgBoardHttpService.getArticle(boardName, articleCid).subscribe(response => {
      this.meta$.next(response['meta']);

      const article = new RgArticle();
      article.cid = response['response']['cid'];
      article.title = response['response']['title'];
      article.description = response['response']['description'];
      article.tagList = response['response']['tag_list'];
      article.fileList = [];
      response['response']['file_list'].forEach(file => {
        const rgFile = new RgFile();
        rgFile.id = file.id;
        rgFile.name = file.filename;
        rgFile.size = file.filesize_mb;
        rgFile.type = file.filetype;
        rgFile.regDate = file.reg_date;
        article.fileList.push(rgFile);
      });
      article.langCode = response['response']['lang_code'];
      const guest = new User();
      guest.uid = response['response']['guest_id'];
      guest.name = response['response']['guest_name'];
      guest.profileImageUrl = response['response']['profile_image_url'];
      article.guest = guest;
      article.regDate = response['response']['reg_date'];
      article.viewCount = response['response']['view_count'];
      this.article$.next(article);
    });
  }
  postArticle(boardName) {
    return this.rgBoardHttpService.postArticle(boardName, this.article$.value);
  }
  putArticle(boardName, articleCid) {
    return this.rgBoardHttpService.putArticle(boardName, articleCid, this.article$.value);
  }
  deleteArticle(boardName, articleCid) {
    return this.rgBoardHttpService.deleteArticle(boardName, articleCid);
  }
  getTagList(boardName, keyword) {
    return this.rgBoardHttpService.getTags(boardName, keyword);
  }
}

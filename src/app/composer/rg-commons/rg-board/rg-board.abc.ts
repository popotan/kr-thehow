namespace abcRgBoard {
    export interface Board {
        id?: number;
        name: string;
        aka: string;
        style: BoardStyle;
        langCode: LangCode;
        permissions: abcRgSession.PermissionType[];
        setLangCode(langCode: string): void;
    }
    export const enum BoardStyle {
        board = 'board',
        thread = 'thread',
        feed = 'feed',
        photo = 'photo'
    }
    export const enum LangCode {
        ko = 'ko',
        en = 'en'
    }
    export abstract class RgBoard implements Board {
        id?: number;
        name: string;
        aka: string;
        style: BoardStyle;
        langCode: LangCode;
        permissions: abcRgSession.PermissionType[];
        public setLangCode(langCode: string): void {}
        setAka(): void {}
    }
    export interface Article {
        cid?: number;
        title: string;
        description: string;
        langCode: string;
        regDate?: string|Date;
    }
    export interface File {
        id?: number;
        name: string;
        size?: number;
        url?: string;
        type?: string;
        realPath?: string;
        regDate?: string|Date;
        getUrl(): any;
    }
    export abstract class RgArticle implements Article {
        cid?: number;
        title: string;
        description: string;
        fileList?: RgFile[];
        langCode: string;
        guest?: abcRgSession.User;
        regDate?: Date;
        replyLength?: number;
        viewCount?: number;
    }
    export abstract class RgFile implements File {
        id?: number;
        name: string;
        size?: number;
        url?: string;
        type?: string;
        realPath?: string;
        regDate?: string|Date;
        getUrl(): any {}
    }
}

import { TestBed } from '@angular/core/testing';

import { RgBoardService } from './rg-board.service';

describe('RgBoardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RgBoardService = TestBed.get(RgBoardService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { RgArticle } from './rg-board';
import { RgBoardHttpService } from './rg-board-http.service';

@Injectable({
  providedIn: 'root'
})
export class RgThreadService {
  article$: BehaviorSubject<RgArticle>;
  constructor(
    private rgBoardHttpService: RgBoardHttpService
  ) { }
}

import { TestBed } from '@angular/core/testing';

import { RgBoardHttpService } from './rg-board-http.service';

describe('RgBoardHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RgBoardHttpService = TestBed.get(RgBoardHttpService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { RgFeedService } from './rg-feed.service';

describe('RgFeedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RgFeedService = TestBed.get(RgFeedService);
    expect(service).toBeTruthy();
  });
});

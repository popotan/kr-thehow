import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { RgBoard, RgArticle, RgFile } from './rg-board';
import { RgBoardHttpService } from './rg-board-http.service';
import { User } from '../rg-session/rg-session';
import { RgSessionService } from '../rg-session/rg-session.service';

@Injectable()
export class RgBoardService {
  boardInfo$: BehaviorSubject<RgBoard> = new BehaviorSubject(null);
  articleList$: BehaviorSubject<RgArticle[]> = new BehaviorSubject([]);
  tagList$: BehaviorSubject<string[]> = new BehaviorSubject([]);
  hasPermission$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  articleCount$: BehaviorSubject<number> = new BehaviorSubject(0);
  constructor(
    private rgSessionService: RgSessionService,
    private rgBoardHttpService: RgBoardHttpService
  ) { }
  getBoardInfo(boardName, langCode?: abcRgBoard.LangCode) {
    this.rgBoardHttpService.getBoardInfo(boardName).subscribe(response => {
      const boardInfo = new RgBoard(response['response']['lang_code']);
      boardInfo.name = response['response']['name'];
      boardInfo.permissions = response['response']['write_auth'];
      boardInfo.style = response['response']['style'];
      if (langCode) {
        boardInfo.setLangCode(langCode);
      }

      this.rgSessionService.user$.subscribe(user => {
        this.hasPermission$.next(false);
        if (user.auth) {
          boardInfo.permissions.forEach(permission => {
            if (user.auth.indexOf(permission) > -1) {
              this.hasPermission$.next(true);
            }
          });
        }
      });

      this.boardInfo$.next(boardInfo);
    });
  }
  getArticleList(boardName, page = 1, keyword?, tag?) {
    this.rgBoardHttpService.getArticleList(boardName, page, keyword, 'article', tag).subscribe(response => {
      this.articleCount$.next(response['meta']['total_count']);
      const responseArticles = [];
      response['response'].forEach(res => {
        const article = new RgArticle();
        article.cid = res.cid;
        article.title = res.title;
        article.tagList = res.tagList;
        article.fileList = [];
        res.file_list.forEach(file => {
          const rgFile = new RgFile();
          rgFile.id = file.id;
          rgFile.name = file.filename;
          rgFile.size = file.filesize_mb;
          rgFile.type = file.filetype;
          rgFile.regDate = file.reg_date;
          article.fileList.push(rgFile);
        });
        article.regDate = res.reg_date;

        const guest = new User();
        guest.name = res.guest_name;
        guest.uid = res.guest_id;
        article.guest = guest;

        responseArticles.push(article);
      });
      this.articleList$.next(responseArticles);
    });
  }
  getTagList(boardName) {
    return this.rgBoardHttpService.getTags(boardName).subscribe(response => {
      if (response['meta']['message'] === 'SUCCESS') {
        this.tagList$.next(response['response']);
      }
    });
  }
}

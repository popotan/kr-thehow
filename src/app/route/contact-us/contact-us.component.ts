import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { apiURL } from 'src/environments/environment.prod';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  contact = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required)
  });
  constructor(
    private $http: HttpClient
  ) { }

  ngOnInit() {
  }

  postArticle() {
    if (this.contact.valid) {
      return this.$http.post(apiURL + `/etc/contact_us/thehow`, this.contact.value)
        .pipe().subscribe(response => {
          alert('메일이 전송되었습니다.');
          this.contact.reset();
        }, error => {
          alert('메일 전송에 문제가 발생하였습니다. 다시 시도해주세요.');
        });
    } else {
      alert('양식에 맞게 작성해주세요.');
    }
  }

}

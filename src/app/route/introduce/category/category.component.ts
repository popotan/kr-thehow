import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { RgArticle } from 'src/app/composer/rg-commons/rg-board/rg-board';
import { RgBoardHttpService } from 'src/app/composer/rg-commons/rg-board/rg-board-http.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
  providers: [RgBoardHttpService]
})
export class CategoryComponent implements OnInit {
  constructor(
    private rgBoardHttpService: RgBoardHttpService
  ) {
  }

  ngOnInit() {
  }

}

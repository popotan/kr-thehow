import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-weare',
  templateUrl: './weare.component.html',
  styleUrls: ['./weare.component.css']
})
export class WeareComponent implements OnInit {
  @ViewChild('sliderTarget') sliderTarget: ElementRef;
  sliderCurIndex = 0;
  constructor(
    private renderer2: Renderer2
  ) { }

  ngOnInit() {
  }

  moveSlider(direction) {
    const childLength = this.sliderTarget.nativeElement.children.length;
    if (direction === 'next') {
      if (this.sliderCurIndex <= childLength) {
        this.sliderCurIndex += 2;
      }
    } else if (direction === 'prev') {
      if (this.sliderCurIndex !== 0) {
        this.sliderCurIndex -= 2;
      }
    }
    const targetMargin = -680 * this.sliderCurIndex;
    this.renderer2.setStyle(this.sliderTarget.nativeElement, 'margin-left', targetMargin + 'px');
  }
}

import { Component, OnInit, AfterViewInit } from '@angular/core';
import { RgCarousel } from 'src/app/composer/rg-commons/rg-carousel/rg-carousel';
import { Observable } from 'rxjs';
import { RgArticle } from 'src/app/composer/rg-commons/rg-board/rg-board';
import { RgBoardService } from 'src/app/composer/rg-commons/rg-board/rg-board.service';
import { HttpClient } from '@angular/common/http';
import { apiURL } from 'src/environments/environment.prod';
import { Validators, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [RgBoardService]
})
export class IndexComponent implements OnInit, AfterViewInit {
  articleList$: Observable<RgArticle[]> = this.rgBoardService.articleList$.asObservable();
  headerCarousel: RgCarousel;
  categoryCarousel: RgCarousel;
  contact = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required)
  });
  constructor(
    private rgBoardService: RgBoardService,
    private $http: HttpClient
  ) { }

  ngOnInit() {
    this.headerCarousel = new RgCarousel();
    this.headerCarousel.cssWidth = '100%';
    this.headerCarousel.cssHeight = '100%';

    this.categoryCarousel = new RgCarousel();
    this.categoryCarousel.cssWidth = '100%';
    this.categoryCarousel.cssHeight = '100%';

    this.getReferenceArticleList();
  }

  ngAfterViewInit() {
  }

  getReferenceArticleList() {
    this.rgBoardService.getArticleList('reference', 1);
  }

  postArticle() {
    if (this.contact.valid) {
      return this.$http.post(apiURL + `/etc/contact_us/thehow`, this.contact.value)
        .pipe().subscribe(response => {
          alert('메일이 전송되었습니다.');
          this.contact.reset();
        }, error => {
          alert('메일 전송에 문제가 발생하였습니다. 다시 시도해주세요.');
        });
    } else {
      alert('양식에 맞게 작성해주세요.');
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { LoginUserForm } from 'src/app/composer/rg-commons/rg-session/rg-session';
import { RgSessionService } from 'src/app/composer/rg-commons/rg-session/rg-session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginUserForm: LoginUserForm;
  constructor(
    private rgSessionService: RgSessionService
  ) {
    this.loginUserForm = new LoginUserForm();
  }

  ngOnInit() {
  }

  submit() {
    this.rgSessionService.login(this.loginUserForm);
  }

}

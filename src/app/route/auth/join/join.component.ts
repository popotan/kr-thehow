import { Component, OnInit } from '@angular/core';
import { JoinUserForm } from 'src/app/composer/rg-commons/rg-session/rg-session';
import { RgSessionService } from 'src/app/composer/rg-commons/rg-session/rg-session.service';

@Component({
  selector: 'app-join',
  templateUrl: './join.component.html',
  styleUrls: ['./join.component.css']
})
export class JoinComponent implements OnInit {
  joinUserForm: JoinUserForm;
  isPasswordMatched = false;
  isVisiblePasswordMathedError = false;
  constructor(
    private rgSessionService: RgSessionService
  ) {
    this.joinUserForm = new JoinUserForm();
  }

  ngOnInit() {
  }

  checkPassword(password_str) {
    this.isVisiblePasswordMathedError = true;
    if (this.joinUserForm.password === password_str) {
      this.isPasswordMatched = true;
    } else {
      this.isPasswordMatched = false;
    }
  }

  submit() {
    if (this.isPasswordMatched) {
      this.rgSessionService.join(this.joinUserForm);
    } else {
      alert('비밀번호가 일치하지 않습니다. 다시 한 번 입력해주세요!');
    }
  }

}

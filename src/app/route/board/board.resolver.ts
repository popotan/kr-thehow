import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { RgBoardService } from 'src/app/composer/rg-commons/rg-board/rg-board.service';

@Injectable()
export class BoardsResolver implements Resolve<any> {
    constructor(
        private rgBoardService: RgBoardService
    ) { }
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot) {
        return this.rgBoardService.getBoardInfo(route.params['boardName']);
    }

}

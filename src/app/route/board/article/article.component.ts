import { Component, OnInit } from '@angular/core';
import { Route, Router, ActivatedRoute } from '@angular/router';
import { RgCommonReaderService } from 'src/app/composer/rg-commons/rg-board/rg-common-reader.service';
import { Observable } from 'rxjs';
import { RgArticle } from 'src/app/composer/rg-commons/rg-board/rg-board';
import { apiURL } from 'src/environments/environment.prod';
import { RgSessionService } from 'src/app/composer/rg-commons/rg-session/rg-session.service';
import { User } from 'src/app/composer/rg-commons/rg-session/rg-session';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css'],
  providers: [RgCommonReaderService]
})
export class ArticleComponent implements OnInit {
  apiURL = apiURL;
  public user$: Observable<User> = this.rgSessionService.user$.asObservable();
  public meta$: Observable<any> = this.rgCommonReaderService.meta$.asObservable();
  public article$: Observable<RgArticle> = this.rgCommonReaderService.article$.asObservable();
  constructor(
    private rgSessionService: RgSessionService,
    private rgCommonReaderService: RgCommonReaderService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.activatedRoute.params.subscribe(params => {
      this.rgCommonReaderService.getArticle(this.activatedRoute.parent.snapshot.params['boardName'], params['articleCid']);
      window.scroll(0, 0);
    });
  }

  ngOnInit() {
  }

  gotoList() {
    this.router.navigate(['../../'], { relativeTo: this.activatedRoute });
  }
  deleteArticle() {
    this.article$.subscribe(article => {
      console.log(article);
      this.rgCommonReaderService.deleteArticle(this.activatedRoute.parent.snapshot.params['boardName'], article.cid).subscribe(response => {
        if (response['meta']['message'] === 'SUCCESS') {
          alert('글이 삭제되었습니다.');
          this.router.navigate(['../../'], { relativeTo: this.activatedRoute });
        }
      });
    });
  }
}

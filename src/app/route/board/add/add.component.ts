import { Component, OnInit } from '@angular/core';
import { RgCommonReaderService } from 'src/app/composer/rg-commons/rg-board/rg-common-reader.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RgArticle, RgBoard } from 'src/app/composer/rg-commons/rg-board/rg-board';
import { RgBoardService } from 'src/app/composer/rg-commons/rg-board/rg-board.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
  providers: [RgBoardService, RgCommonReaderService]
})
export class AddComponent implements OnInit {
  public boardInfo$: Observable<RgBoard> = this.rgBoardService.boardInfo$.asObservable();
  public article$: Observable<RgArticle> = this.rgCommonReaderService.article$.asObservable();
  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public rgBoardService: RgBoardService,
    public rgCommonReaderService: RgCommonReaderService
  ) {
    this.activatedRoute.params.subscribe(params => {
      this.rgBoardService.getBoardInfo(params['boardName']);
      const newArticle = new RgArticle();
      newArticle.title = '';
      newArticle.description = '';
      newArticle.fileList = [];
      newArticle.langCode = 'ko';
      this.rgCommonReaderService.article$.next(newArticle);
    });
  }

  ngOnInit() {
  }

  previewArticle() {

  }
  cachingArticle() {

  }
  submit() {
    this.rgCommonReaderService.postArticle(this.activatedRoute.snapshot.params['boardName']).subscribe(response => {
      if (response['meta']['message'] === 'SUCCESS') {
        alert('작성하신 글이 등록되었습니다.');
        this.router.navigate(['/board', this.activatedRoute.snapshot.params['boardName'], 'article', response['meta']['cid']]);
      }
    });
  }
}

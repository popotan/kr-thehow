import { Component, OnInit } from '@angular/core';
import { RgBoardService } from 'src/app/composer/rg-commons/rg-board/rg-board.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { RgBoard, RgArticle } from 'src/app/composer/rg-commons/rg-board/rg-board';
import { User } from 'src/app/composer/rg-commons/rg-session/rg-session';
import { RgSessionService } from 'src/app/composer/rg-commons/rg-session/rg-session.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css'],
  providers: [RgBoardService]
})
export class BoardComponent implements OnInit {
  user$: Observable<User> = this.rgSessionService.user$.asObservable();
  boardInfo$: Observable<RgBoard> = this.rgBoardService.boardInfo$.asObservable();
  articleList$: Observable<RgArticle[]> = this.rgBoardService.articleList$.asObservable();
  tagList$: Observable<string[]> = this.rgBoardService.tagList$.asObservable();
  hasPermission$: Observable<boolean> = this.rgBoardService.hasPermission$.asObservable();
  articleCount$: Observable<number> = this.rgBoardService.articleCount$.asObservable();
  queryParams = {};
  pagination = [];
  currentPage = 1;
  lastPage = 1;

  constructor(
    private rgSessionService: RgSessionService,
    private rgBoardService: RgBoardService,
    private router: Router,
    public activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.queryParams.subscribe(qparams => {
      this.queryParams = qparams;
      if (qparams['page']) {
        this.currentPage = Number(qparams['page']);
      }
      this.rgBoardService.getBoardInfo(
        this.activatedRoute.snapshot.params['boardName'],
        abcRgBoard.LangCode.ko);
      this.rgBoardService.getArticleList(
        this.activatedRoute.snapshot.params['boardName'],
        qparams['page'],
        qparams['keyword'],
        qparams['tag']);
      this.rgBoardService.getTagList(
        this.activatedRoute.snapshot.params['boardName']);
    });
    this.articleList$.subscribe(articleList => {
      this.setPagination();
    });
  }

  ngOnInit() {
  }

  setQueryParams(qparamName, qparamValue) {
    const presentQparams = this.activatedRoute.snapshot.queryParams;

    const receivedQparams = {};
    receivedQparams[qparamName] = qparamValue;

    return Object.assign({}, presentQparams, receivedQparams);
  }

  search(keyword) {
    const nextQparams = this.setQueryParams('keyword', keyword);
    this.router.navigate(['./'], { relativeTo: this.activatedRoute, queryParams: nextQparams });
  }

  searchByTag(tag) {
    const nextQparams = this.setQueryParams('tag', tag);
    this.router.navigate(['./'], { relativeTo: this.activatedRoute, queryParams: nextQparams });
  }

  setPagination() {
    const final = [];
    this.articleCount$.subscribe(articleCount => {
      this.lastPage = Math.ceil(articleCount / 10);
      const leftMargin = this.currentPage - 2;
      let rightMargin = this.currentPage + 2;

      for (let i = leftMargin; i <= rightMargin; i++) {
        if (i < 1) {
          rightMargin++;
        }
        if (i > 0 && i <= this.lastPage) {
          final.push(i);
        } else if (i > this.lastPage) {
          if (final.length > 0 && final[0] > 1) {
            final.unshift(final[0] - 1);
          }
        }
      }
      this.pagination = final;
    }).unsubscribe();
  }
}

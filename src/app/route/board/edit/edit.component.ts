import { Component, OnInit } from '@angular/core';
import { RgArticle } from 'src/app/composer/rg-commons/rg-board/rg-board';
import { ActivatedRoute, Router } from '@angular/router';
import { RgBoardService } from 'src/app/composer/rg-commons/rg-board/rg-board.service';
import { RgCommonReaderService } from 'src/app/composer/rg-commons/rg-board/rg-common-reader.service';
import { AddComponent } from '../add/add.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css'],
  providers: [RgBoardService, RgCommonReaderService]
})
export class EditComponent extends AddComponent implements OnInit {
  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public rgBoardService: RgBoardService,
    public rgCommonReaderService: RgCommonReaderService
  ) {
    super(router, activatedRoute, rgBoardService, rgCommonReaderService);
    this.activatedRoute.params.subscribe(params => {
      this.rgCommonReaderService.getArticle(this.activatedRoute.snapshot.params['boardName'], params['articleCid']);
    });
  }

  ngOnInit() {
  }
  submit() {
    this.rgCommonReaderService.putArticle(
      this.activatedRoute.snapshot.params['boardName'],
      this.activatedRoute.snapshot.params['articleCid'])
      .subscribe(response => {
        if (response['meta']['message'] === 'SUCCESS') {
          alert('작성하신 글이 수정되었습니다.');
          this.router.navigate(['/board', this.activatedRoute.snapshot.params['boardName'], 'article', response['meta']['cid']]);
        }
      });
  }
}

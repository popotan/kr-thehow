export const environment = {
  production: true
};

export const apiURL = `http://${window.location.host}/api`;
